# Example 2: Given a sorted array of unique integers and a target integer, return true if there exists a pair of numbers that sum to target, false otherwise.
# This problem is similar to Two Sum. (In Two Sum, the input is not sorted).
# For example, given nums = [1, 2, 4, 6, 8, 9, 14, 15] and target = 13, return true because 4 + 9 = 13.


# Algorithm: two pointers
# Complexity: O(n)

def does_array_have_two_nums_that_sum_to_target(arr: list, target: int) -> bool:
    left_pointer = 0
    right_pointer = len(arr) - 1

    while left_pointer < right_pointer:
        check_sum = arr[left_pointer] + arr[right_pointer]

        if check_sum == target:
            return True
        
        if check_sum > target:
            right_pointer -= 1

        if check_sum < target:
            left_pointer += 1


    return False


assert does_array_have_two_nums_that_sum_to_target([], 12) == False

assert does_array_have_two_nums_that_sum_to_target([1, 2, 4, 6, 8, 9, 14, 15], 13) == True

assert does_array_have_two_nums_that_sum_to_target([1, 2, 4, 6, 8, 9, 14, 15], 10) == True

assert does_array_have_two_nums_that_sum_to_target([1, 2, 4, 6, 8, 9, 14, 15], 112210) == False

assert does_array_have_two_nums_that_sum_to_target([15], 15) == False