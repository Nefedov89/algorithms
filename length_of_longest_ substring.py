# You are given a binary string s (a string containing only "0" and "1"). You may choose up to one "0" and flip it to a "1". What is the length of the longest substring achievable that contains only "1"?
# For example, given s = "1101100111", the answer is 5. If you perform the flip at index 2, the string becomes 1111100111.

# Algorithm: sliding window
# Complexity: O(n)

def len_of_longest_substring(binary_string: str) -> int:
    left_index = right_index = curr_amount_of_zeros = 0
    answer = 0
    
    for char in binary_string:
        if char == '0':
            curr_amount_of_zeros += 1
            
        while curr_amount_of_zeros > 1:
            if binary_string[left_index] == '0':
                curr_amount_of_zeros -= 1
            left_index += 1
        
        answer = max(answer, right_index - left_index + 1)
        
        right_index += 1
        
    return answer
    
    
assert len_of_longest_substring('1101100111') == 5
assert len_of_longest_substring('11001011') == 4