# Given two strings s and t, return true if s is a subsequence of t, or false otherwise.

# Algorithm: two pointers
# Complexity: O(n + m)
def is_subsequence(s: str, t: str) -> bool:
    i = j = 0
    
    while i < len(s) and j < len(t):
        if s[i] == t[j]:
            i += 1
        j += 1
        
    return i == len(s)
    

assert is_subsequence('ace', 'dafcme') == True
assert is_subsequence('ace', 'dafcmp') == False