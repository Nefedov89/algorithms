<?php

declare(strict_types = 1);

/**
 * @param array $array
 *
 * @return array
 */
function quickSort(array $array): array
{
    if (count($array) < 2) {
        return $array;
    } else {
        $pivot = array_shift($array);
        $less = [];
        $greater = [];

        foreach ($array as $v) {
            if ($v > $pivot) {
                $greater[] = $v;
            } else {
                $less[] = $v;
            }
        }

        return array_merge(quickSort($less), [$pivot], quickSort($greater));
    }
}

var_dump(implode(',', quickSort([10, 5, 16, 3, 11, 24])));