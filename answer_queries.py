# Example 1: Given an integer array nums, an array queries where queries[i] = [x, y] and an integer limit, return a boolean array that represents the answer to each query. A query is true if the sum of the subarray from x to y is less than limit, or false otherwise.

# For example, given nums = [1, 6, 3, 2, 7, 2], queries = [[0, 3], [2, 5], [2, 4]], and limit = 13, the answer is [true, false, true]. For each query, the subarray sums are [12, 14, 12].

# Algorithm: prefix sum https://leetcode.com/explore/interview/card/leetcodes-interview-crash-course-data-structures-and-algorithms/703/arraystrings/4503/
# Complexity: O(n)

def answer_queries(nums, queries, limit) -> list:
    prefix_sum = [nums[0]]
    ans = []

    for i in range(1, len(nums)):
        prefix_sum.append(nums[i] + prefix_sum[-1])

    for x, y in queries:
        current = prefix_sum[y] - prefix_sum[x] + nums[x]
        ans.append(current < limit)
    
    return ans


assert answer_queries([1, 6, 3, 2, 7, 2], [[0, 3], [2, 5], [2, 4]], 13) == [True, False, True]