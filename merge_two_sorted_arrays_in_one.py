# Algorithm: two pointers
# Complexity: O(n + m) = O(n)

def merge_two_sorted_arrays_in_one(arr1: list, arr2: list) -> list:
    res = []

    i = j = 0

    while i < len(arr1) and j < len(arr2):
        if arr1[i] < arr2[j]:
            res.append(arr1[i])
            i += 1
        else:
            res.append(arr2[j])
            j += 1
        
    while i < len(arr1):
        res.append(arr1[i])
        i += 1

    while j < len(arr2):
        res.append(arr2[j])
        j += 1

    return res  


assert merge_two_sorted_arrays_in_one([1, 3, 5], [2, 4, 6]) == [1, 2, 3, 4, 5, 6]
assert merge_two_sorted_arrays_in_one([], [2, 4, 6]) == [2, 4, 6]
assert merge_two_sorted_arrays_in_one([1, 3, 5], []) == [1, 3, 5]
assert merge_two_sorted_arrays_in_one([], []) == []
assert merge_two_sorted_arrays_in_one([2, 11], [1, 999]) == [1, 2, 11, 999]
