import math

# stack
def is_palindrome_stack(string: str) -> bool:
    len_of_string = len(string)
    center_index = math.floor(len_of_string / 2)
    stack = []

    for index, char in enumerate(string):
        if len_of_string % 2 != 0 and index == center_index:
            continue

        if index < center_index:
            stack.append(char)
        else:
            char_to_compare = stack.pop()

            if char_to_compare != char:
                return False

    return not len(stack)

# two pointers
def is_palindrome_two_pointers(string: str) -> bool:
    left_pointer = 0
    right_pointer = len(string) - 1
    
    while left_pointer < right_pointer:
        if string[left_pointer] != string[right_pointer]:
            return False
            
        left_pointer += 1
        right_pointer -= 1
    
    return True
        


test_cases = {'abba': True, 'level': True, '': True, 'tree': False}

for string, is_palindrome in test_cases.items():
    assert is_palindrome_stack(string) == is_palindrome
    assert is_palindrome_two_pointers(string) == is_palindrome