# Let's say that we are given a positive integer array nums and an integer k. 
# We need to find the length of the longest subarray that has a sum less than or equal to k. 
# For this example, let nums = [3, 2, 1, 3, 1, 1] and k = 5.


# Algorithm: sliding window
# Complexity: O(n)

def len_of_longest_subarray_with_sum_less_than(arr: list, k: int) -> int: 
    left_index = right_index = 0
    current_sum = 0
    length_of_subarr = 0

    while right_index < len(arr):
        current_sum += arr[right_index]

        while current_sum > k:
            current_sum -= arr[left_index]
            left_index += 1

        length_of_subarr = max(length_of_subarr, right_index - left_index + 1)
        right_index += 1

    return length_of_subarr 



assert len_of_longest_subarray_with_sum_less_than([3, 2, 1, 3, 1, 1], 5) == 3
assert len_of_longest_subarray_with_sum_less_than([3, 2, 1, 3, 1, 1], 3) == 2
assert len_of_longest_subarray_with_sum_less_than([3, 2, 1, 3, 1, 1, 2], 7) == 4