def insertion_sort(data_list):
    for i in range(2, len(data_list)):
        j = i

        while j and data_list[j - 1] > data_list[j]:
            data_list[j - 1], data_list[j] = data_list[j], data_list[j - 1]
            j -= 1


data_list = [1, 3, 2, 5, 6, 8, 7, 9, 11, 10, 12]

insertion_sort(data_list)
