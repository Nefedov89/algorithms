# Given an array of positive integers nums and an integer k, return the number of subarrays where the product of all the elements in the subarray is strictly less than k.
# For example, given the input nums = [10, 5, 2, 6], k = 100, the answer is 8. The subarrays with products less than k are:
# [10], [5], [2], [6], [10, 5], [5, 2], [2, 6], [5, 2, 6]

# Algorithm: sliding window
# Complexity: O(n)

class Solution:
    def numSubarrayProductLessThanK(self, nums: list[int], k: int) -> int:
        if k <= 1:
            return 0

        num_of_arrs = 0
        left_index = 0
        product_of_numbers_in_sub_arr = 0
        
        for right_index in range(len(nums)):
            if product_of_numbers_in_sub_arr == 0:
                product_of_numbers_in_sub_arr += nums[right_index]
            else:
                product_of_numbers_in_sub_arr *= nums[right_index]

            while product_of_numbers_in_sub_arr >= k:
                product_of_numbers_in_sub_arr /= nums[left_index]
                left_index += 1
            
            num_of_arrs += right_index - left_index + 1

        return num_of_arrs
        

solution = Solution()

assert solution.numSubarrayProductLessThanK([10, 5, 2, 6], 100) == 8
assert solution.numSubarrayProductLessThanK([1, 2, 3], 0) == 0

    