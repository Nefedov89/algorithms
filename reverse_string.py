# Write a function that reverses a string. The input string is given as an array of characters s.

# You must do this by modifying the input array in-place with O(1) extra memory.

# Algorithm: two pointers
# Complexity: O(n)

def reverse_string(s: list[str]) -> None:
        """
        Do not return anything, modify s in-place instead.
        """
        left_index = 0 
        right_index = len(s) - 1
        
        while left_index < right_index:
            left_value = s[left_index]
            right_value = s[right_index]
            
            s[left_index] = right_value
            s[right_index] = left_value
            
            left_index += 1
            right_index -= 1

arr1 = ["h","e","l","l","o"]
arr2 = ["H","a","n","n","a","h"]

reverse_string(arr1)
reverse_string(arr2)

assert arr1 == ["o","l","l","e","h"]
assert arr2 == ["h","a","n","n","a","H"]