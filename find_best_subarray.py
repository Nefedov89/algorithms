# Example 4: Given an integer array nums and an integer k, 
# find the sum of the subarray with the largest sum whose length is k.

# Algorithm: two pointers (fixed window size)
# Complexity: O(n)

def find_best_subarray(arr: list[int], k: int) -> int:
    arr_len = len(arr)
    if k > arr_len:
        return 0

    current_largest_sum = 0
    right_index = k - 1

    # create largest_sum for first k elements (build a window of first k els)
    for i in range(k):
        current_largest_sum += arr[i]

    answer = current_largest_sum
    for i in range(k, arr_len):
        # slide fixed size window to the right
        current_largest_sum = current_largest_sum + arr[i] - arr[i - k] # add el to right and then remove el from left

        answer = max(answer, current_largest_sum)

    return answer


assert find_best_subarray([1, 2, 3, 4, 5], 3) == 12
assert find_best_subarray([-1, -2, 3, 4, -5], 2) == 7
assert find_best_subarray([3, -1, 4, 12, -8, 5, 6], 4) == 18