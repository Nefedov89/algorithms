<?php

declare(strict_types = 1);

$arr1 = [[4,1],[2,0],[0,1]];
$arr2 = [[0,0],[1,0],[2,0]];

/**
 * @param $arr1
 * @param $arr2
 *
 * @return array
 */
function arrIntersection($arr1, $arr2): array
{
    $result = [];

    foreach ($arr1 as $subArr1) {
        if (in_array($subArr1, $arr2)) {
            $result[] = $subArr1;
        }
    }

    return $result;
}

var_dump(arrIntersection($arr1, $arr2));
