# Given an integer array nums sorted in non-decreasing order,
# return an array of the squares of each number sorted in non-decreasing order.

# Input: nums = [-4,-1,0,3,10]
# Output: [0,1,9,16,100]
# Explanation: After squaring, the array becomes [16,1,0,9,100].
# After sorting, it becomes [0,1,9,16,100].

# Algorithm: two pointers
# Complexity: O(n)

def sorted_squares(nums: list) -> list:
        left_index = 0
        right_index = len(nums) - 1
        res = []
        
        while left_index <= right_index:
            left_num = abs(nums[left_index])
            right_num = abs(nums[right_index])
            
            if right_num > left_num:
                res = [right_num ** 2] + res
                right_index -= 1
               
            else:
                res = [left_num ** 2] + res
                left_index += 1
        
        
        return res


assert sorted_squares([-4,-1,0,3,10]) == [0,1,9,16,100]
assert sorted_squares([-7,-3,2,3,11]) == [4,9,9,49,121]
assert sorted_squares([1,2,3,4,5]) == [1,4,9,16,25]
