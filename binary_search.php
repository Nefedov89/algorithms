<?php

declare(strict_types = 1);

/**
 * @param array $array
 * @param int $item
 *
 * @return int|null
 */
function binarySearch(array $array, int $item): ?int
{
    $low = 0;
    $high = count($array) - 1;

    while ($low <= $high) {
        $mid = (int) round(($low + $high) / 2);
        $guess = $array[$mid];

        if ($guess === $item) {
            return $mid;
        }

        if ($guess > $item) {
            $high = $mid - 1;
        } else {
            $low = $mid + 1;
        }
    }

    return null;
}

var_dump(binarySearch([1, 2, 3, 4, 5, 22, 33, 44], 3));