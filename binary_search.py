import math

def binary_search(arr: list, num: int) -> int:
    left_pointer_index = 0
    right_pointer_index = len(arr) - 1
    
    while left_pointer_index <= right_pointer_index:
        guess_pointer_index = math.floor((right_pointer_index + left_pointer_index) / 2)
        guess = arr[guess_pointer_index]
        
        if guess == num:
            return guess_pointer_index
            
        if guess > num:
            right_pointer_index = guess_pointer_index - 1
            
        if guess < num:
            left_pointer_index = guess_pointer_index + 1
            
    return None
        
    
#      0  1  2  3  4   5    6
arr = [1, 2, 4, 7, 99, 111, 555]
n1 = 3
n2 = 7
n3 = 111
n4 = 99


assert binary_search(arr, n1) is None
assert binary_search(arr, n2) == 3
assert binary_search(arr, n3) == 5
assert binary_search(arr, n4) == 4
assert binary_search([], n4) is None